import axios, {CancelTokenSource} from 'axios'
import React, {useEffect, useState} from 'react'
import './App.scss';
import {TxType} from "./common/types/blockchain-api/tx.type";
import {PaginationType} from "./common/types/pagination.type";
import {RawAddrType} from "./common/types/blockchain-api/raw-addr.type";
import {BalanceType} from "./common/types/balance.type";
import HighchartsReact from "highcharts-react-official";
import * as Highcharts from 'highcharts';
import {Chart, PointOptionsType} from 'highcharts';

const WAValidator = require('@swyftx/api-crypto-address-validator')

const _unit = 'BTC';

function renderTxs(txs?: Array<TxType>): Array<JSX.Element> {
    if (txs && txs.length > 0) {
        return txs.map((tx: TxType, index: number) => (
            <Tx key={index} tx={tx}/>
        ));
    } else return [];
}

function formatAmount(amount: number, unit: string): string {
    return (amount / Math.pow(10, 8)).toFixed(8) + ' ' + unit;
}

const Tx = ({tx}: { tx: TxType }): JSX.Element => {
    const txDate = new Date(tx.time * 1000);
    return (<tr>
        <td>{txDate.toLocaleDateString() + ' ' + txDate.toLocaleTimeString()}</td>
        <td>{tx.hash}</td>
        <td style={{color: tx.result < 0 ? 'red' : 'green'}}>{formatAmount(tx.result, _unit)}</td>
    </tr>);
};

function App(): JSX.Element {
    const [address, setAddress] = useState<string>('');
    const [APIIsLive, setAPIIsLive] = useState<boolean>(false);
    const [addressIsValid, setAddressIsValid] = useState<boolean>(false);
    const [error, setError] = useState<{ occurred: boolean, message?: string }>({occurred: false});
    const [rawAddrData, setRawAddrData] = useState<RawAddrType>();
    const [txs, setTxs] = useState<PaginationType<TxType>>({count: 0, offset: 0, fullyLoaded: false, data: undefined});
    const [computedFinalBalance, setComputedFinalBalance] = useState<number>();
    const [chart, setChart] = useState<Chart>();
    const [chartData, setChartData] = useState<Array<PointOptionsType>>();

    useState(() => {
        axios.get('http://localhost:8080/ping')
            .then(resp => setAPIIsLive(resp.data === 'pong'))
            .catch(err => setAPIIsLive(false))
    })

    useEffect(() => {
        setAddressIsValid(WAValidator.validate(address, _unit));
    }, [address]);

    let cancelTokenSource: CancelTokenSource;
    let cancelLoadNextTxBlock: CancelTokenSource;
    const refresh = () => {
        if (addressIsValid) {
            setError({occurred: false});
            setRawAddrData(undefined);
            setTxs({count: 0, offset: 0, fullyLoaded: false, data: undefined});
            setComputedFinalBalance(undefined);
            cancelLoadNextTxBlock?.cancel();
            cancelTokenSource?.cancel();
            cancelTokenSource = axios.CancelToken.source();
            axios.get<RawAddrType>(`http://localhost:8080/blockchain-api/address/${address}?limit=50&offset=0`, {
                cancelToken: cancelTokenSource.token
            }).then(response => {
                setAddress('');
                setRawAddrData(response.data);
                setTxs((_txs) => {
                    const txs = new PaginationType<TxType>();
                    txs.count = _txs.count + response.data.txs.length;
                    txs.fullyLoaded = !(txs.count < response.data.n_tx);
                    txs.offset = txs.fullyLoaded ? _txs.offset : _txs.offset + 50;
                    txs.data = response.data.txs;
                    const _chartData = getChartData(txs.data, response.data.final_balance);
                    setChartData(_chartData);
                    setChart((_chart) => {
                        if (_chart) {
                            _chart.series[0].setData(_chartData, true);
                        }
                        return _chart;
                    });
                    return txs;
                });
            }).catch((error) => {
                setError({occurred: true, message: error.message});
                console.error(error);
            });
            axios.get<BalanceType>(`http://localhost:8080/blockchain-api/address/${address}/final-balance`, {
                //cancelToken: cancelTokenSource.token
            }).then(response => setComputedFinalBalance(response.data.computedFinalBalance)).catch(error => {
                setError({occurred: true, message: error.message});
                console.error(error)
            });
        }
    };

    const loadNextTxBlock = () => {
        if (txs.fullyLoaded || !rawAddrData) {
            return;
        }
        cancelLoadNextTxBlock = axios.CancelToken.source();
        axios.get<RawAddrType>(`http://localhost:8080/blockchain-api/address/${rawAddrData.address}?limit=50&offset=${txs.offset}`, {
            cancelToken: cancelLoadNextTxBlock.token
        }).then(response => {
            setTxs((_txs: PaginationType<TxType>) => {
                const txs = new PaginationType<TxType>();
                txs.count = _txs.count + response.data.txs.length;
                txs.fullyLoaded = !(txs.count < response.data.n_tx);
                txs.offset = txs.fullyLoaded ? _txs.offset : _txs.offset + 50;
                txs.data = _txs.data?.concat(response.data.txs);
                if (txs.data) {
                    const _chartData = getChartData(txs.data, response.data.final_balance);
                    setChartData(_chartData);
                    setChart((_chart) => {
                        if (_chart) {
                            _chart.series[0].setData(_chartData, true);
                        }
                        return _chart;
                    });
                }
                return txs;
            })
        }).catch(error => console.error(error));
    }

    const handleScroll = (e: React.UIEvent<HTMLTableElement>): void => {
        const element = e.currentTarget;
        if (element.scrollHeight - element.scrollTop === element.clientHeight) {
            loadNextTxBlock();
        }
    }

    const getChartData = (txs: Array<TxType>, finalBalance: number): Array<{ x: number, y: number }> => {
        const mergedTxs: any = {};
        txs.forEach((tx) => {
            mergedTxs[tx.time.toString()] = mergedTxs[tx.time.toString()] ? mergedTxs[tx.time.toString()] + tx.result : tx.result;
        });
        return Object.keys(mergedTxs).map((key) => ({
            x: new Date(parseInt(key) * 1000).getTime(),
            y: mergedTxs[key],
            evolution: 0
        })).sort((a, b) => {
            if (a.x > b.x) {
                return -1;
            }
            if (a.x < b.x) {
                return 1;
            }
            return 0;
        }).map((point, i, points) => {
            point.evolution = points.slice(0, i + 1).map(p => p.y).reduce((a, b) => a + b);
            return point;
        }).map((point) => ({x: point.x, y: (finalBalance - point.evolution) / Math.pow(10, 8)})).sort((a, b) => {
            if (a.x > b.x) {
                return 1;
            }
            if (a.x < b.x) {
                return -1;
            }
            return 0;
        }).concat({x: new Date().getTime(), y: finalBalance / Math.pow(10, 8)});
    }

    let highchartsOptions: Highcharts.Options = {
        chart: {
            className: 'chart',
            backgroundColor: 'transparent'
        },
        title: {
            text: 'Bitcoin Balance',
            useHTML: true,
            style: {
                color: 'white',
                fontSize: '16px'
            },
        },
        xAxis: {
            type: 'datetime',
            labels: {
                useHTML: true,
                rotation: 60,
                style: {
                    color: 'white',
                    fontSize: '16px'
                },
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: undefined
            },
            labels: {
                style: {
                    color: 'white',
                    fontSize: '16px'
                }
            }
        },
        legend: {
            useHTML: true,
            itemStyle: {
                color: 'white'
            },
            title: {
                style: {
                    color: 'white',
                    fontSize: '16px'
                }
            }
        },
        tooltip: {
            valueSuffix: ' ' + _unit
        },
        series: [{
            className: 'adjusted-forecasts',
            color: '#ea973d',
            type: 'line',
            zIndex: 1,
            name: 'Bitcoin Balance',
        }],
        credits: {
            enabled: false
        }
    };

    return (
        <div className='main'>
            <div className='header'>
                <h1><img
                    src={process.env.PUBLIC_URL + "/assets/bitcoin.svg"}
                    height="100pxpx"
                    width="100px"/> Bitcoin Explorer</h1>
                <div className='entry-container'>
                    <div className='entry'>
                        <input
                            type='text'
                            placeholder='Bitcoin address...'
                            value={address}
                            onChange={e => setAddress(e.target.value)}
                            onKeyPress={(event) => {
                                event.key === 'Enter' && refresh()
                            }}
                        />
                        <input
                            style={{background: 'url(' + process.env.PUBLIC_URL + "/assets/look_up.svg" + ') no-repeat 20px 10px'}}
                            disabled={!addressIsValid} type='button' onClick={refresh} value={''}/>
                    </div>
                    {!addressIsValid && !!address &&
                    <span style={{color: 'red', marginTop: '10px'}}>Invalid Bitcoin address</span>}
                    {error && error.occurred &&
                    <span style={{color: 'red', marginTop: '10px'}}>API returned an error : {error.message}</span>}
                </div>
            </div>
            {
                rawAddrData &&
                <div className='body'>
                    <table className='main-infos'>
                        <tbody>
                        <tr>
                            <td>Address</td>
                            <td>{rawAddrData.address}</td>
                        </tr>
                        <tr>
                            <td>Transactions</td>
                            <td>{rawAddrData.n_tx}</td>
                        </tr>
                        <tr>
                            <td>Total Received</td>
                            <td>{formatAmount(rawAddrData.total_received, _unit)}</td>
                        </tr>
                        <tr>
                            <td>Total Sent</td>
                            <td>{formatAmount(rawAddrData.total_sent, _unit)}</td>
                        </tr>
                        <tr>
                            <td>Final Balance</td>
                            <td>{formatAmount(rawAddrData.final_balance, _unit)}</td>
                        </tr>
                        <tr>
                            <td>Computed Final Balance</td>
                            <td>{computedFinalBalance != undefined ? formatAmount(computedFinalBalance, _unit) : 'Unknown'}</td>
                        </tr>
                        </tbody>
                    </table>
                    <div className='transactions-container'>
                        <div className='chart-container'>
                            <HighchartsReact
                                highcharts={Highcharts}
                                options={highchartsOptions}
                                callback={(chart: Chart) => {
                                    setChart(chart);
                                    setChartData((_chartData) => {
                                        if (_chartData) {
                                            chart.series[0].setData(_chartData);
                                        }
                                        return _chartData;
                                    });
                                }}
                            />
                        </div>
                        <table onScroll={handleScroll}>
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Hash</th>
                                <th>Amount</th>
                            </tr>
                            </thead>
                            <tbody>
                            {renderTxs(txs.data)}
                            </tbody>
                            <tfoot>
                            <tr>
                                <td>{'Display ' + txs.count + ' of ' + rawAddrData.n_tx + ' transactions'}</td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            }
            <div className='footer'>
                {APIIsLive ? <span>🟢 API Connected</span> : <span style={{color: 'red'}}>🔴 API Failure</span>}
            </div>
        </div>
    )
}

export default App
