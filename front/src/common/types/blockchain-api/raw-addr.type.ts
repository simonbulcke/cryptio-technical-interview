import {TxType} from "./tx.type";

export class RawAddrType {
    hash160: string;
    address: string;
    n_tx: number;
    total_received: number;
    total_sent: number;
    final_balance: number;
    txs: Array<TxType>;
}