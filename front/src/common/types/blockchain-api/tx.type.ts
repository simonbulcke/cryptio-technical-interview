export class TxType {
    ver: number;
    // inputs: Array<>;
    weight: number;
    block_height: number;
    relayed_by: string;
    // out: "";
    lock_time: number;
    result: number;
    size: number;
    block_index: number;
    time: number;
    tx_index: number;
    vin_sz: number;
    hash: string;
    vout_sz: number;
}