export class PaginationType<T> {
    count: number;
    offset: number;
    fullyLoaded: boolean;
    data: Array<T> | undefined;
}
