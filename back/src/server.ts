import App from './app';
import validateEnv from './utils/validate-env';
import BlockchainApiRoute from "./routes/blockchain-api.route";
import DefaultRoute from "./routes/default.route";

validateEnv();
const app = new App([new DefaultRoute(), new BlockchainApiRoute()]);

app.listen();
