import {Router} from 'express';
import Route from '../interfaces/routes.interface';
import BlockchainApiController from '../controllers/blockchain-api.controller';

class BlockchainApiRoute implements Route {
    public path = '/blockchain-api';
    public router = Router();
    public blockchainApiController = new BlockchainApiController();

    constructor() {
        this.initializeRoutes();
    }

    private initializeRoutes() {
        this.router.get(`${this.path}/address/:address`, this.blockchainApiController.getDetails);
        this.router.get(`${this.path}/address/:address/final-balance`, this.blockchainApiController.getFinalBalance);
    }
}

export default BlockchainApiRoute;
