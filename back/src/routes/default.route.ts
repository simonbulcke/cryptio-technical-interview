import {Router} from 'express';
import Route from '../interfaces/routes.interface';
import DefaultController from "../controllers/default.controller";

class DefaultRoute implements Route {
    public path = '';
    public router = Router();
    public defaultController = new DefaultController();

    constructor() {
        this.initializeRoutes();
    }

    private initializeRoutes() {
        this.router.get(`${this.path}/ping`, this.defaultController.ping);
    }
}

export default DefaultRoute;
