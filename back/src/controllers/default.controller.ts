import {NextFunction, Request, Response} from "express";

class DefaultController {
    public ping(req: Request, res: Response, next: NextFunction): void {
        res.status(200).send('pong');
    };
}

export default DefaultController;
