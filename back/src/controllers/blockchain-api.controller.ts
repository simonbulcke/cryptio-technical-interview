import {NextFunction, Request, Response} from 'express';

import CacheController from "./cache.controller";
import {RawAddrType} from "../../../front/src/common/types/blockchain-api/raw-addr.type";

class BlockchainApiController {

    public getDetails(req: Request, res: Response, next: NextFunction): Promise<void> {
        return CacheController.get<RawAddrType>(`https://blockchain.info/rawaddr/${req.params.address}?limit=${req.query.limit}&offset=${req.query.offset}`)
            .then(response => {
                res.status(200).json(response.data);
            })
            .catch(error => next(error));
    };

    private static _computeFinalBalance(address: string, initBalance: number, offset: number): Promise<number> {
        return new Promise((resolve, reject) => {
            CacheController.get<RawAddrType>(`https://blockchain.info/rawaddr/${address}?limit=50&offset=${offset}`).then(response => {
                offset += 50;
                const tmpBalance = initBalance + response.data.txs.map((tx) => tx.result).reduce((a, b) => a + b);
                if (response.data.n_tx > offset) {
                    BlockchainApiController._computeFinalBalance(address, tmpBalance, offset).then(response =>
                        resolve(response)
                    ).catch(error => reject(error));
                } else {
                    resolve(tmpBalance);
                }
            }).catch(error => reject(error))
        });
    }

    public getFinalBalance(req: Request, res: Response, next: NextFunction): Promise<void> {
        return BlockchainApiController._computeFinalBalance(req.params.address, 0, 0).then(response => {
            res.status(200).json({computedFinalBalance: response});
        }).catch(error => {
            next(error);
        });
    };
}

export default BlockchainApiController;
