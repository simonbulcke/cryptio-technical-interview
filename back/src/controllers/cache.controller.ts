import axios, {AxiosResponse} from "axios";

const MemoryCache = require('memory-cache');

class CacheController<T> {
    static get<T>(url: string): Promise<AxiosResponse<T>> {
        return new Promise<AxiosResponse<T>>((resolve, reject) => {
            const cachedResponse = MemoryCache.get(url)
            if (cachedResponse) {
                resolve(cachedResponse);
            } else {
                axios.get<T>(url).then((response) => {
                    MemoryCache.put(url, response, 3600000) // responses are cached for 1 hour
                    resolve(response)
                }).catch((error) => reject(error));
            }
        });
    };
}

export default CacheController;
